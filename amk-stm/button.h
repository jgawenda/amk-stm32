#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "stm32f10x_gpio.h"

#define BUTTON_Pin GPIO_Pin_13
#define BUTTON_Bus GPIOC

/* @brief: initializes buttons and interrupts for it
*/
void Button_Init();

/* @brief: disables interrupts for the button
*/
void Button_Disable();

/* @brief: enables interrupts for the button
*/
void Button_Enable();

#endif