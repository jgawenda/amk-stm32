#include "lcd.h"
#include "stm32f10x.h"
#include <stdint.h>

static __IO uint32_t usTicks; 

void SysTick_Handler()
{
    if (usTicks != 0)
    {
     usTicks--;
    }
}

void usDelay(uint32_t us)
{
    usTicks = us;
    
    while(usTicks)
    {
    }
}

void LCD_Send4Bits(uint8_t nibble)
{
    // sets bits and generates a falling edge for the LCD
    GPIO_WriteBit(LCD_D7_Bus, LCD_D7_Pin, (nibble & 0x08) ? Bit_SET : Bit_RESET );
    GPIO_WriteBit(LCD_D6_Bus, LCD_D6_Pin, (nibble & 0x04) ? Bit_SET : Bit_RESET );
    GPIO_WriteBit(LCD_D5_Bus, LCD_D5_Pin, (nibble & 0x02) ? Bit_SET : Bit_RESET );
    GPIO_WriteBit(LCD_D4_Bus, LCD_D4_Pin, (nibble & 0x01) ? Bit_SET : Bit_RESET );
    GPIO_WriteBit(LCD_E_Bus, LCD_E_Pin, Bit_SET);
    usDelay(900);
    GPIO_WriteBit(LCD_E_Bus, LCD_E_Pin, Bit_RESET);
    usDelay(900);


}

void LCD_Send4BitCommand(uint8_t command)
{
    GPIO_WriteBit(LCD_RS_Bus, LCD_RS_Pin, Bit_RESET);
    LCD_Send4Bits(command & 0x0f);
}

void LCD_SendCommand(uint8_t command)
{
    GPIO_WriteBit(LCD_RS_Bus, LCD_RS_Pin, Bit_RESET);            
    LCD_Send4Bits(command >> 4);
    LCD_Send4Bits(command & 0x0f);
}

void LCD_SendData(uint8_t data)
{
    GPIO_WriteBit(LCD_RS_Bus, LCD_RS_Pin, Bit_SET);
    LCD_Send4Bits(data >> 4);
    LCD_Send4Bits(data & 0x0f);
}

void LCD_InitPins()
{
    GPIO_InitTypeDef GPIOHandler;
    //clock enabling
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);
    
    // initializing GPIO structure
    GPIO_StructInit(&GPIOHandler);
    
    // initializing all pins
    GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
    GPIOHandler.GPIO_Pin = LCD_E_Pin;
    GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(LCD_E_Bus, &GPIOHandler);
    GPIO_WriteBit(LCD_E_Bus, LCD_E_Pin, Bit_RESET);
    
    GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
    GPIOHandler.GPIO_Pin = LCD_D7_Pin;
    GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(LCD_D7_Bus, &GPIOHandler);
    GPIO_WriteBit(LCD_D7_Bus, LCD_D7_Pin, Bit_RESET);
    
    GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
    GPIOHandler.GPIO_Pin = LCD_D6_Pin;
    GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(LCD_D6_Bus, &GPIOHandler);
    GPIO_WriteBit(LCD_D6_Bus, LCD_D6_Pin, Bit_RESET);
    
    GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
    GPIOHandler.GPIO_Pin = LCD_D5_Pin;
    GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(LCD_D5_Bus, &GPIOHandler);
    GPIO_WriteBit(LCD_D5_Bus, LCD_D5_Pin, Bit_RESET);
    
    GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
    GPIOHandler.GPIO_Pin = LCD_D4_Pin;
    GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(LCD_D4_Bus, &GPIOHandler);
    GPIO_WriteBit(LCD_D4_Bus, LCD_D4_Pin, Bit_RESET);
    
    GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
    GPIOHandler.GPIO_Pin = LCD_RS_Pin;
    GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(LCD_RS_Bus, &GPIOHandler);
    GPIO_WriteBit(LCD_RS_Bus, LCD_RS_Pin, Bit_RESET);
}

void LCD_InitDisplay()
{
    // sending 0x03 three times to configure the LCD for 4 bit mode
    usDelay(40000);
    LCD_Send4BitCommand(LCD_INIT_CMD);
    
    usDelay(5000);
    LCD_Send4BitCommand(LCD_INIT_CMD);
    
    usDelay(2000);
    LCD_Send4BitCommand(LCD_INIT_CMD);
    
    // getting cursor into (0, 0) position
    LCD_Send4BitCommand(LCD_RETURN_HOME);
    usDelay(1000);
    
    // enabling 2 lines with small font
    LCD_SendCommand(LCD_FUNCTION_SET | LCD_2LINE | LCD_SMALL_FONT);
    
    // turning off display, cursor and blinking
    LCD_SendCommand(LCD_DISPLAY_CONTROL | LCD_DISPLAY_OFF | LCD_CURSOR_OFF | LCD_BLINKING_OFF);
    
    // clearing display
    LCD_SendCommand(LCD_CLEAR_DISPLAY);
    
    // configuring cursor to shift to the right after every character
    LCD_SendCommand(LCD_ENTRY_MODE_SET | LCD_CURSOR_SHIFT_RIGHT);
    
    // turning the display on
    LCD_SendCommand(LCD_DISPLAY_CONTROL | LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINKING_OFF);
}

void LCD_SetCursor(uint8_t posX, uint8_t posY)
{
    uint8_t rowOffset[] = {0x00, 0x40};
    
    LCD_SendCommand(LCD_SET_DDRAM | (posX + rowOffset[posY]));
}

void LCD_SendString(char *string, uint8_t length)
{
    for (uint8_t i = 0; i < length; i++)
    {
        LCD_SendData(string[i]);
    }
}

void LCD_PrintBoard(char *upBoard, char *downBoard)
{
    LCD_SetCursor(0, 0);
    LCD_SendString(upBoard, 16);
    LCD_SetCursor(0, 1);
    LCD_SendString(downBoard, 16);
}

void Delay_Init()
{
    SysTick_Config(SystemCoreClock / 1000000);
}