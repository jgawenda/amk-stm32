#include "Game.h"
#include "lcd.h"
#include "button.h"

Player player;

static uint32_t maxPoints;
static uint32_t gamePoints;


/* @brief:  inform player about loss,
 *          clear what needed before next game,
 *          save points
 */
static void endGame()
{

    char boardUp[BOARD_LENGTH];
    char boardDown[BOARD_LENGTH];
    
    sprintf(boardUp, "    You lost!   ");
    sprintf(boardDown, "Points: %*d", 8, gamePoints);
    if (gamePoints > maxPoints)
    {
        maxPoints = gamePoints;
    }
    
    LCD_PrintBoard(boardUp, boardDown);
    
    gamePoints = 0;
    clearEnemies();

    usDelay(1000000);
}

bool checkCollision()
{
    Enemy *headEnemy = getHeadEnemy();
    if (headEnemy)
    {
        if (headEnemy -> positionX == 0)
        {
            if (headEnemy -> positionY == player.positionY)
            {
                deleteEnemy();
                return true;
            }
            else
            {
                gamePoints++;
            }
            deleteEnemy();
            return false;
        }
        return false;
    }
    return false;
}

void makeBoard()
{
    // board
    Button_Disable();
    char boardUp[BOARD_LENGTH];
    char boardDown[BOARD_LENGTH];
    memset(boardUp, '-', sizeof(boardUp));
    memset(boardDown, '-', sizeof(boardDown));

    if (player.positionY == 0)
    {
        boardUp[0] = 'B';
    }
    else
    {
        boardDown[0] = 'B';
    }

    printEnemiesBoard(boardUp, 0);
    printEnemiesBoard(boardDown, 1);

    
    LCD_PrintBoard(boardUp, boardDown);
    Button_Enable();
    
}

void menu()
{
    // board
    char boardUp[BOARD_LENGTH];
    char boardDown[BOARD_LENGTH];
    char boardDown_[BOARD_LENGTH];
    sprintf(boardUp,    "      GIERA     ");
    sprintf(boardDown,  " CLICK TO SWITCH");
    sprintf(boardDown_, "MAX POINTS: %*d", 4, maxPoints);

    int i  = 3;
    bool flipBoardDown = false;
    while (i)
    {
        if (flipBoardDown)
        {
            LCD_PrintBoard(boardUp, boardDown);
            flipBoardDown = !flipBoardDown;
        }
        else
        {
            LCD_PrintBoard(boardUp, boardDown_);
            flipBoardDown = !flipBoardDown;
        }

        usDelay(1000000);
        i--;
    }

}

void startGame()
{
    initializePlayer(&player);

    while (true)
    {
        addRandomEnemy();
        moveEnemies();
        makeBoard();
        if (checkCollision())
        {
            endGame();
            break;
        }
        usDelay(400000);
    }
}