#include "stm32f10x.h"
#include "lcd.h"
#include "button.h"
#include "Game.h"
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

extern Player player;

void EXTI15_10_IRQHandler()
{
    if (EXTI_GetITStatus(EXTI_Line13)) {
    
        movePlayer(&player);
        makeBoard();
        EXTI_ClearITPendingBit(EXTI_Line13);
    }
}

int main(void)
{

    SystemCoreClockUpdate();

    Delay_Init();
    LCD_InitPins();
    LCD_InitDisplay();
    Button_Init();

    while(1)
    {
        menu();
        startGame();
    }
    return 0;
}

