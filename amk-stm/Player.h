#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

/* @param playerChar:	a character given to the player
 * @param positionX:	column of the player. Defaults to 0
 * @param positionY:	line of the player, can be {0, 1}
 */
typedef struct Player
{
	char playerChar;
	uint8_t positionX;
	uint8_t positionY;
} Player;

/* @brief:	initializes Player structure, gives an icon and starting position
 *			tbh, the playerChar could be yeeted
 */
void initializePlayer(Player *player);

/* @brief: moves player to the other line
 * @param player:	pointer to the Player structure, representing the player
 */
void movePlayer(Player *player);

#endif
