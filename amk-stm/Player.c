#include "Player.h"

void initializePlayer(Player *player)
{
    player -> playerChar = 'B';
    player -> positionX = 0;
    player -> positionY = 0;
}

void movePlayer(Player *player)
{
    player -> positionY = player -> positionY ? 0 : 1;
}
