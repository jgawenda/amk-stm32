#ifndef _GAME_H_
#define _GAME_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "Enemy.h"
#include "Player.h"

#define BOARD_LENGTH 17
#define BOARD_WIDTH 2

/* @brief:	check for the collision with the Player.
 *			if the collision occurs, the game ends.
 *			add point if enemy passed
 */
bool checkCollision();

/* @brief:	generates the board and prints it to the console
 *			will need to be updated to work with the 2x16 LCD
 */
void makeBoard();

/* @brief:	main game menu, prints information for a player
 *			and blocks until player clicks button
 */
void menu();

/* @brief:  main game logic, take inputs from a player
 *          blocks until player losses, print information at the and
 */
void startGame();

void playerMove(Player *player);

#endif