#include "button.h"
#include <stm32f10x.h>

void Button_Init()
{

    // enable clock for Port C and alternate functions
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    
    // initialize PC 13 as input (with pull up resistor)
    GPIO_InitTypeDef GPIOHandler;
    GPIO_StructInit(&GPIOHandler);
    
    // configuration for PC 13
    GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
    GPIOHandler.GPIO_Pin = BUTTON_Pin;
    GPIOHandler.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(BUTTON_Bus, &GPIOHandler);
    
    // configure PC 13 as external interrupt source
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource13);
    
    // initialize EXTI
    EXTI_InitTypeDef EXTIInit;

        // EXTI structure initialization
        EXTI_StructInit(&EXTIInit);
    
        EXTIInit.EXTI_Line = EXTI_Line13;
        EXTIInit.EXTI_Mode = EXTI_Mode_Interrupt;
        EXTIInit.EXTI_Trigger = EXTI_Trigger_Falling;
        EXTIInit.EXTI_LineCmd = ENABLE;
    
        EXTI_Init(&EXTIInit);
    
    // NVIC structure initialization
    NVIC_InitTypeDef NVICInit;
    
        NVICInit.NVIC_IRQChannel = EXTI15_10_IRQn;
        NVICInit.NVIC_IRQChannelPreemptionPriority = 0x00;
        NVICInit.NVIC_IRQChannelSubPriority = 0x00;
        NVICInit.NVIC_IRQChannelCmd = ENABLE;
    
        NVIC_Init(&NVICInit);
 
    // setting priority for nesting interrupts (SysTick has higher priority than EXTI_Line13)
    NVIC_SetPriorityGrouping(NVIC_PriorityGroup_4);
    NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(4, 0, 0));
    NVIC_SetPriority(EXTI15_10_IRQn, NVIC_EncodePriority(4, 15, 0));
}

void Button_Disable()
{

    // disables button interrupt
    NVIC_InitTypeDef NVICInit;
    
        NVICInit.NVIC_IRQChannel = EXTI15_10_IRQn;
        NVICInit.NVIC_IRQChannelPreemptionPriority = 0x00;
        NVICInit.NVIC_IRQChannelSubPriority = 0x00;
        NVICInit.NVIC_IRQChannelCmd = DISABLE;
    
        NVIC_Init(&NVICInit);
 
    // set priority (SysTick has higher priority than EXTI_Line13)
    NVIC_SetPriorityGrouping(NVIC_PriorityGroup_4);
    NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(4, 0, 0));
    NVIC_SetPriority(EXTI15_10_IRQn, NVIC_EncodePriority(4, 15, 0));
}

void Button_Enable()
{
    // enable button interrupt
    NVIC_InitTypeDef NVICInit;
    
        NVICInit.NVIC_IRQChannel = EXTI15_10_IRQn;
        NVICInit.NVIC_IRQChannelPreemptionPriority = 0x00;
        NVICInit.NVIC_IRQChannelSubPriority = 0x00;
        NVICInit.NVIC_IRQChannelCmd = ENABLE;
    
        NVIC_Init(&NVICInit);
 
    // set priority (SysTick has higher priority than EXTI_Line13)
    NVIC_SetPriorityGrouping(NVIC_PriorityGroup_4);
    NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(4, 0, 0));
    NVIC_SetPriority(EXTI15_10_IRQn, NVIC_EncodePriority(4, 15, 0));
}