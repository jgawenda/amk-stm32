#ifndef _LCD_H_
#define _LCD_H_

#include <stdint.h>

#define LCD_RS_Pin GPIO_Pin_12
#define LCD_E_Pin GPIO_Pin_11
#define LCD_D7_Pin GPIO_Pin_13
#define LCD_D6_Pin GPIO_Pin_14
#define LCD_D5_Pin GPIO_Pin_15
#define LCD_D4_Pin GPIO_Pin_1

#define LCD_RS_Bus GPIOB
#define LCD_E_Bus GPIOB
#define LCD_D7_Bus GPIOB
#define LCD_D6_Bus GPIOB
#define LCD_D5_Bus GPIOB
#define LCD_D4_Bus GPIOB

#define LCD_CLEAR_DISPLAY 0x01
#define LCD_RETURN_HOME 0x02
#define LCD_ENTRY_MODE_SET 0x04
#define LCD_DISPLAY_CONTROL 0x08
#define LCD_CURSOR_SHIFT 0x10
#define LCD_FUNCTION_SET 0x20
#define LCD_SET_CGRAM 0x40
#define LCD_SET_DDRAM 0x80

// ENTRY MODE SET CONFIGS
#define LCD_CURSOR_SHIFT_RIGHT 0x02
#define LCD_CURSOR_SHIFT_LEFT 0x00
#define LCD_DISPLAY_SHIFT 0x01

// DISPLAY CONTROL CONFIGS
#define LCD_DISPLAY_OFF 0x00
#define LCD_DISPLAY_ON 0x04
#define LCD_CURSOR_OFF 0x00
#define LCD_CURSOR_ON 0x02
#define LCD_BLINKING_OFF 0x00
#define LCD_BLINKING_ON 0x01

// CURSOR SHIFT
#define LCD_MOVE_CURSOR 0x00
#define LCD_MOVE_LEFT 0x00
#define LCD_MOVE_RIGHT 0x04

// FUNCTION SET
#define LCD_4BIT 0x00
#define LCD_8BIT 0x10
#define LCD_1LINE 0x00
#define LCD_2LINE 0x08
#define LCD_SMALL_FONT 0x00
#define LCD_LARGE_FONT 0x04

#define LCD_INIT_CMD 0x03


/* @brief: configures interrupt handler for SysTick
*/
void SysTick_Handler();

/* @brief: delay function
 * @param us: number of microseconds to wait
 */
void usDelay(uint32_t us);

/* @brief: initializes SysTick
 */
void Delay_Init();

/* @brief: initializes LCD pins
 */
void LCD_InitPins();

/* @brief: initializes LCD display
 */
void LCD_InitDisplay();

/* @brief: sends four bits to LCD
 * @param nibble: four bits to be sent
 */
void LCD_Send4Bits(uint8_t nibble);

/* @brief: sends four bit command (sets RS bit to LOW)
 * @param command: four bit command
 */
void LCD_Send4BitCommand(uint8_t command);

/* @brief: sends an 8-bit command, split into two 4-bit sets
 * @param command: 8-bit command
 */
void LCD_SendCommand(uint8_t command);

/* @brief: sends character data to LCD (sets RS bit to HIGH)
 * @param data: ASCII code for a character
 */
void LCD_SendData(uint8_t data);

/* @brief: sets LCD cursor at given coords
 * @param posX: X position of the cursor
 * @param posY: Y position of the cursor (upper line = 0, bottom line = 1)
 */
void LCD_SetCursor(uint8_t posX, uint8_t posY);

/* @brief: writes string onto LCD
 * @param string: pointer to char array
 * @param length: length of the string (number of chars to be written)
 */
void LCD_SendString(char *string, uint8_t length);

/* @brief: prints upper and lower line onto LCD
 * @param upBoard: char array for the upper line
 * @param downBoard: char array for the lower line
 */
void LCD_PrintBoard(char *upBoard, char *downBoard);

#endif