#ifndef _ENEMY_H_
#define _ENEMY_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>
#include "Game.h"

/* @param enemyChar:	icon of the enemy
 * @param positionX:	column of the enemy, starts at 15; decreases each game tick
 * @param positionY:	line of the enemy, can be either {0, 1}
 * @param *nextEnemy:	pointer to the next enemy in the list. Used for a dynamic number of enemies
 */
typedef struct Enemy
{
	char enemyChar;
	int8_t positionX;
	uint8_t positionY;
	struct Enemy *nextEnemy;
} Enemy;

/* @brief:  adds (or not) enemy on random position Y at the end of the board
 *          position depends on earlier enemy, to make it possible for player to win
 */
bool addRandomEnemy();

/* @brief: deletes the first enemy in the list. The implementation guarantees, that it's the one on position (0, y)
 */
void deleteEnemy();

/* @brief:	moves each enemy one tile to the left (x, y) -> (x - 1, y).
 *			if the enemy reaches player column, it checks for the collision.
 *			if the collision occurs, the game ends.
 */
void moveEnemies();

/* @brief:	returns the pointer to the first enemy in the list
 */
Enemy* getHeadEnemy();

/* @brief:	generates a board to display
 * @param board:	17-char array, containing graphics for one line
 * @param posY:		number of line, for which the board is drawn
 *					0 - upper line, 1 - lower line
 */
void printEnemiesBoard(char *board, uint8_t posY);

/*  @brief: delete all enemies, needed after player lost
 *
 */
void clearEnemies();

#endif
