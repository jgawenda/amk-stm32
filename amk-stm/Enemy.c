#include "Enemy.h"

// standard includes - pointer to the first enemy in the list and declaration of the Player struct
static Enemy *headEnemy = NULL;

/* @brief: this function adds enemies to dodge. Each enemy starts at the last column (16th) on a semi-random line
 * @param posY: line on which the enemy is located. Will be incorporated into the function
 */
static bool addEnemy(uint8_t posY)
{
    if (headEnemy == NULL)
    {
        headEnemy = malloc(sizeof(*headEnemy));
        headEnemy -> enemyChar = '#';
        headEnemy -> positionX = 15;
        headEnemy -> positionY = posY;
        headEnemy -> nextEnemy = NULL;
        return true;
    }
    else
    {
        Enemy *iterator = headEnemy;
        while (iterator -> nextEnemy != NULL)
        {
            iterator = iterator -> nextEnemy;
        }
        iterator -> nextEnemy = malloc(sizeof(*headEnemy));
        iterator -> nextEnemy -> enemyChar = '#';
        iterator -> nextEnemy -> positionX = 15;
        iterator -> nextEnemy -> positionY = posY;
        iterator -> nextEnemy -> nextEnemy = NULL;
        return true;
    }
}

/* @brief:    prints the x position of every existing enemy. Used for debugging purposes
 */
static void printEnemies()
{
    Enemy *iterator = headEnemy;

    if (headEnemy == NULL)
    {
        printf("No enemies remain!\n");
    }

    while (iterator != NULL)
    {
        printf("%d\n", iterator -> positionX);
        iterator = iterator -> nextEnemy;
    }
}

bool addRandomEnemy()
{
    // function can make enemy or not, more possible that it would
    if (rand() % 2)
    {
        return false;
    }

    uint8_t randomPosY = rand() % 2;

    if (headEnemy == NULL)
    {
        addEnemy(randomPosY);
        return true;
    }
    Enemy *iterator = headEnemy;

    while (iterator->nextEnemy != NULL)
    {
        iterator = iterator->nextEnemy;
    }

    // find when it would be possible to win with enemies and change randomly chosen position
    // rand in if and else to make bigger spaces then 1, sometimes
    if (iterator->positionX == 14)
    {
        if (iterator->positionY == randomPosY)
        {
            addEnemy(randomPosY);
        }
        else
        {
            addEnemy(1 - randomPosY);
        }
    }
    else
    {
        addEnemy(randomPosY);
    }
    return true;
}

void deleteEnemy()
{
    Enemy *oldEnemy = headEnemy;
    headEnemy = oldEnemy -> nextEnemy;
    free(oldEnemy);
}

void moveEnemies()
{
    Enemy *iterator = headEnemy;

    while (iterator != NULL)
    {
        iterator->positionX--;
        iterator = iterator->nextEnemy;
    }
}

Enemy* getHeadEnemy()
{
    return headEnemy;
}

void printEnemiesBoard(char board[], uint8_t posY)
{
    Enemy *iterator = headEnemy;

    while(iterator)
    {
        if (iterator -> positionY == posY)
        {
            board[iterator -> positionX] = '#';
        }
        iterator = iterator -> nextEnemy;
    }
    board[16] = '\0';
}

void clearEnemies()
{
    Enemy *iterator = headEnemy;

    while(iterator)
    {
        iterator = iterator -> nextEnemy;
        deleteEnemy();
    }
}
