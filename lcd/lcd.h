#ifndef _LCD_H_
#define _LCD_H_

#include <stdint.h>

#define LCD_RS_Pin GPIO_Pin_12
#define LCD_E_Pin GPIO_Pin_11
#define LCD_D7_Pin GPIO_Pin_13
#define LCD_D6_Pin GPIO_Pin_14
#define LCD_D5_Pin GPIO_Pin_15
#define LCD_D4_Pin GPIO_Pin_1

#define LCD_RS_Bus GPIOB
#define LCD_E_Bus GPIOB
#define LCD_D7_Bus GPIOB
#define LCD_D6_Bus GPIOB
#define LCD_D5_Bus GPIOB
#define LCD_D4_Bus GPIOB

#define LCD_CLEAR_DISPLAY 0x01
#define LCD_RETURN_HOME 0x02
#define LCD_ENTRY_MODE_SET 0x04
#define LCD_DISPLAY_CONTROL 0x08
#define LCD_CURSOR_SHIFT 0x10
#define LCD_FUNCTION_SET 0x20
#define LCD_SET_CGRAM 0x40
#define LCD_SET_DDRAM 0x80

// ENTRY MODE SET CONFIGS
#define LCD_CURSOR_SHIFT_RIGHT 0x02
#define LCD_CURSOR_SHIFT_LEFT 0x00
#define LCD_DISPLAY_SHIFT 0x01

// DISPLAY CONTROL CONFIGS
#define LCD_DISPLAY_OFF 0x00
#define LCD_DISPLAY_ON 0x04
#define LCD_CURSOR_OFF 0x00
#define LCD_CURSOR_ON 0x02
#define LCD_BLINKING_OFF 0x00
#define LCD_BLINKING_ON 0x01

// CURSOR SHIFT
#define LCD_MOVE_CURSOR 0x00
#define LCD_MOVE_LEFT 0x00
#define LCD_MOVE_RIGHT 0x04

// FUNCTION SET
#define LCD_4BIT 0x00
#define LCD_8BIT 0x10
#define LCD_1LINE 0x00
#define LCD_2LINE 0x08
#define LCD_SMALL_FONT 0x00
#define LCD_LARGE_FONT 0x04

#define LCD_INIT_CMD 0x03

void SysTick_Handler();
void usDelay();
void Delay_Init();
void LCD_InitPins();
void LCD_InitDisplay();
void LCD_Send4Bits(uint8_t nibble);
void LCD_Send4BitCommand(uint8_t command);
void LCD_SendCommand(uint8_t command);
void LCD_SendData(uint8_t data);

#endif