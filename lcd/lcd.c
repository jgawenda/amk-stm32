#include "lcd.h"
#include "stm32f10x.h"
#include <stdint.h>

static __IO uint32_t usTicks; 

void SysTick_Handler()
{
	if (usTicks != 0)
	{
	 usTicks--;
	}
}

void usDelay(uint32_t us)
{
	usTicks = us;
	
	while(usTicks)
	{
	}
}

void LCD_Send4Bits(uint8_t nibble)
{

	GPIO_WriteBit(LCD_D7_Bus, LCD_D7_Pin, (nibble & 0x08) ? Bit_SET : Bit_RESET );
	GPIO_WriteBit(LCD_D6_Bus, LCD_D6_Pin, (nibble & 0x04) ? Bit_SET : Bit_RESET );
	GPIO_WriteBit(LCD_D5_Bus, LCD_D5_Pin, (nibble & 0x02) ? Bit_SET : Bit_RESET );
	GPIO_WriteBit(LCD_D4_Bus, LCD_D4_Pin, (nibble & 0x01) ? Bit_SET : Bit_RESET );
	GPIO_WriteBit(LCD_E_Bus, LCD_E_Pin, Bit_SET);
	usDelay(1000);
	GPIO_WriteBit(LCD_E_Bus, LCD_E_Pin, Bit_RESET);
	usDelay(1000);


}

void LCD_Send4BitCommand(uint8_t command)
{
	GPIO_WriteBit(LCD_RS_Bus, LCD_RS_Pin, Bit_RESET);
	LCD_Send4Bits(command & 0x0f);
}

void LCD_SendCommand(uint8_t command)
{
	GPIO_WriteBit(LCD_RS_Bus, LCD_RS_Pin, Bit_RESET);
	LCD_Send4Bits(command >> 4);
	LCD_Send4Bits(command & 0x0f);
}

void LCD_SendData(uint8_t data)
{
	GPIO_WriteBit(LCD_RS_Bus, LCD_RS_Pin, Bit_SET);
	LCD_Send4Bits(data >> 4);
	LCD_Send4Bits(data & 0x0f);
}

void LCD_InitPins()
{
	GPIO_InitTypeDef GPIOHandler;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);
	
	GPIO_StructInit(&GPIOHandler);
	
	GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOHandler.GPIO_Pin = LCD_E_Pin;
	GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LCD_E_Bus, &GPIOHandler);
	GPIO_WriteBit(LCD_E_Bus, LCD_E_Pin, Bit_RESET);
	
	GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOHandler.GPIO_Pin = LCD_D7_Pin;
	GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LCD_D7_Bus, &GPIOHandler);
	GPIO_WriteBit(LCD_D7_Bus, LCD_D7_Pin, Bit_RESET);
	
	GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOHandler.GPIO_Pin = LCD_D6_Pin;
	GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LCD_D6_Bus, &GPIOHandler);
	GPIO_WriteBit(LCD_D6_Bus, LCD_D6_Pin, Bit_RESET);
	
	GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOHandler.GPIO_Pin = LCD_D5_Pin;
	GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LCD_D5_Bus, &GPIOHandler);
	GPIO_WriteBit(LCD_D5_Bus, LCD_D5_Pin, Bit_RESET);
	
	GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOHandler.GPIO_Pin = LCD_D4_Pin;
	GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LCD_D4_Bus, &GPIOHandler);
	GPIO_WriteBit(LCD_D4_Bus, LCD_D4_Pin, Bit_RESET);
	
	GPIOHandler.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOHandler.GPIO_Pin = LCD_RS_Pin;
	GPIOHandler.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LCD_RS_Bus, &GPIOHandler);
	GPIO_WriteBit(LCD_RS_Bus, LCD_RS_Pin, Bit_RESET);
}

void LCD_InitDisplay()
{
	usDelay(40000);
	LCD_Send4BitCommand(LCD_INIT_CMD);
	
	usDelay(5000);
	LCD_Send4BitCommand(LCD_INIT_CMD);
	
	usDelay(2000);
	LCD_Send4BitCommand(LCD_INIT_CMD);
	
	LCD_Send4BitCommand(LCD_RETURN_HOME);
	usDelay(1000);
	
	LCD_SendCommand(LCD_FUNCTION_SET | LCD_2LINE | LCD_SMALL_FONT);
	LCD_SendCommand(LCD_DISPLAY_CONTROL | LCD_DISPLAY_OFF | LCD_CURSOR_OFF | LCD_BLINKING_OFF);
	LCD_SendCommand(LCD_CLEAR_DISPLAY);
	LCD_SendCommand(LCD_ENTRY_MODE_SET | LCD_CURSOR_SHIFT_RIGHT);
	LCD_SendCommand(LCD_DISPLAY_CONTROL | LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINKING_OFF);
	
}

void Delay_Init()
{
	SysTick_Config(SystemCoreClock / 1000000);
}