#include "stm32f10x.h"
#include "lcd.h"
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>



/** delay()
  * @brief waits a given amount of time
  * @param time: amount of ms to wait
  */
void delay(int time)
{
	int i;
	for (i = 0; i < time*8000; i++);
}


/*
void LCD_Send4Bits(uint8_t nibble)
{
	GPIO_WriteBit(BUS_D7, LED_D7_Pin, (nibble & 0x08) ? Bit_SET : Bit_RESET );
	GPIO_WriteBit(BUS_D6, LED_D6_Pin, (nibble & 0x04) ? Bit_SET : Bit_RESET );
	GPIO_WriteBit(BUS_D5, LED_D5_Pin, (nibble & 0x02) ? Bit_SET : Bit_RESET );
	GPIO_WriteBit(BUS_D4, LED_D4_Pin, (nibble & 0x01) ? Bit_SET : Bit_RESET );
	
	GPIO_WriteBit(BUS_E, LED_E_Pin, Bit_SET);
	usDelay(100);
	GPIO_WriteBit(BUS_E, LED_E_Pin, Bit_RESET);
	usDelay(100);
}*/



int main(void)
{

	SystemCoreClockUpdate();

	Delay_Init();
	LCD_InitPins();
	LCD_InitDisplay();

	// infinite loop for collecting user input
	while(1)
	{
		LCD_SendData('#');
		usDelay(250000);
	}
	return 0;
}

