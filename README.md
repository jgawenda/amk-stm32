# AMK-STM32

Twórcy: Jakub Gawenda, Łukasz Hajec.

Repozytorium projektu na AMK (prosta gra).

Dostępne płytki ewaluacyjne: 
  - STM32: Nucleo F103RB i Nucleo F302R8,
  - ATmega328P: Arduino UNO.

 Dostępne części:
  - wyświetlacz: 2x16 LCD (jeden z ekspanderem I2C, drugi bez),
  - przyciski,
  - buzzery,
  - diody,
  - etc.

TODO:
  - napisanie (lub znalezienie i wykorzystanie) biblioteki do wyświetlacza LCD,
  - napisanie gry,
  - integracja obu części.